package com.example.dos_6.historiappzgz.util;

import uk.me.jstott.jcoord.*;

/**
 * Created by dos_6 on 15/12/2015.
 */
public class Util {

    public static LatLng DeUMTSaLatLng(double este, double oeste, char zonaLat, int zonaLong) {

        UTMRef utm = new UTMRef(este, oeste, 'N', 30);

        return utm.toLatLng();
    }
}
