package com.example.dos_6.historiappzgz.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dos_6.historiappzgz.Monumento;
import com.example.dos_6.historiappzgz.R;

import java.util.List;

/**
 * Created by dos_6 on 15/12/2015.
 */
public class MonumentoAdapter extends ArrayAdapter<Monumento>{

    private Context contexto;
    private int layoutId;
    private List<Monumento> datos;

    public MonumentoAdapter(Context contexto, int layoutId, List<Monumento> datos) {
        super(contexto, layoutId, datos);

        this.contexto = contexto;
        this.layoutId = layoutId;
        this.datos = datos;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        View fila = view;
        ItemMonumento item = null;

        if (fila == null) {
            LayoutInflater inflater = ((Activity) contexto).getLayoutInflater();
            fila = inflater.inflate(layoutId, padre, false);

            item = new ItemMonumento();
            item.nombre = (TextView) fila.findViewById(R.id.tvNombre);
            item.direccion = (TextView) fila.findViewById(R.id.tvDireccion);


            fila.setTag(item);
        }
        else {
            item = (ItemMonumento) fila.getTag();
        }

        Monumento monumento = datos.get(posicion);
        item.nombre.setText(monumento.getNombre());
        item.direccion.setText(monumento.getDescripcion());

        return fila;
    }

    static class ItemMonumento {

        TextView nombre;
        TextView direccion;
    }

}
