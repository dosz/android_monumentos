package com.example.dos_6.historiappzgz.main;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.dos_6.historiappzgz.Monumento;
import com.example.dos_6.historiappzgz.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Login extends ActionBarActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(this);
        Button btRegistro = (Button) findViewById(R.id.btRegistro);
        btRegistro.setOnClickListener(this);

    }



    public void onClick(View view) {

        Intent intent;

        switch (view.getId()) {

            case R.id.btLogin:
                intent = new Intent(this, MonumentosMain.class);
                startActivity(intent);
                break;
            case R.id.btRegistro:
                intent = new Intent(this, Registro.class);
                startActivity(intent);
        }

    }

}
